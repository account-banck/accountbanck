INSERT INTO account (account_id, balance, owner_name, account_number, account_type, CREATED, updated)
VALUES ('123e4567-e89b-12d3-a456-556642440000', 1000.00, 'John Doe', '123456789', 'Checking', CURRENT_TIMESTAMP,
        CURRENT_TIMESTAMP),
       ('223e4567-e89b-12d3-a456-556642440001', 2500.50, 'Jane Smith', '987654321', 'Savings', CURRENT_TIMESTAMP,
        CURRENT_TIMESTAMP);

INSERT  INTO  transaction (transaction_id, account_id, date, amount, description, type, CREATED, updated)
VALUES ('323e4567-e89b-12d3-a456-556642440002', '123e4567-e89b-12d3-a456-556642440000', '2023-01-15', 500.00,
        'Dépôt initial', 'DEPOT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
       ('423e4567-e89b-12d3-a456-556642440003', '223e4567-e89b-12d3-a456-556642440001', '2023-01-20', 100.50,
        'Dépôt initial', 'DEPOT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
