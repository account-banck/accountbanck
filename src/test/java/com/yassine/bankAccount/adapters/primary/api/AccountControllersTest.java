package com.yassine.bankAccount.adapters.primary.api;

import com.yassine.bankAccount.application.api.AccountControllers;
import com.yassine.bankAccount.application.dtos.AccountDto;
import com.yassine.bankAccount.application.dtos.TransactionDto;
import com.yassine.bankAccount.application.mapper.generics.AccountDtoMapper;
import com.yassine.bankAccount.application.mapper.generics.TransactionDtoMapper;
import com.yassine.bankAccount.common.MapperService;
import com.yassine.bankAccount.domain.model.AccountData;
import com.yassine.bankAccount.domain.model.TransactionData;
import com.yassine.bankAccount.application.incoming.AccountService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@WebMvcTest(AccountControllers.class)
class AccountControllersTest {

  private final ObjectMapper objectMapper = new ObjectMapper();
  private MockMvc mockMvc;

  @MockBean private AccountService accountService;

  @MockBean private MapperService mapperService;

  @BeforeEach
  void init() {
    objectMapper.registerModule(new JavaTimeModule());

    AccountDtoMapper accountDtoMapper = Mockito.mock(AccountDtoMapper.class);
    TransactionDtoMapper transactionDtoMapper = Mockito.mock(TransactionDtoMapper.class);

    Mockito.when(mapperService.getByType(AccountDtoMapper.class)).thenReturn(accountDtoMapper);
    Mockito.when(accountDtoMapper.toDomain(Mockito.any(AccountDto.class)))
        .thenReturn(new AccountData());
    Mockito.when(accountDtoMapper.toDto((AccountData) Mockito.any())).thenReturn(new AccountDto());

    Mockito.when(mapperService.getByType(TransactionDtoMapper.class))
        .thenReturn(transactionDtoMapper);
    Mockito.when(transactionDtoMapper.toDto((TransactionData) Mockito.any()))
        .thenReturn((new TransactionDto()));
    Mockito.when(transactionDtoMapper.toDto(Mockito.anyList()))
        .thenReturn(List.of(new TransactionDto()));

    AccountControllers accountControllers = new AccountControllers(accountService, mapperService);
    mockMvc = MockMvcBuilders.standaloneSetup(accountControllers).build();
  }

  @Test
  void testCreateAccount() throws Exception {
    var accountDto = new AccountDto();
    String accountDtoJson = convertToJson(accountDto);

    mockMvc
        .perform(
            MockMvcRequestBuilders.post("/accounts")
                .contentType(MediaType.APPLICATION_JSON)
                .content(accountDtoJson))
        .andExpect(MockMvcResultMatchers.status().isCreated());
  }

  @Test
  void testDeposit() throws Exception {
    var transactionDto = new TransactionDto();
    String transactionJson = convertToJson(transactionDto);

    mockMvc
        .perform(
            MockMvcRequestBuilders.post("/accounts/{accountId}/deposit", UUID.randomUUID())
                .contentType(MediaType.APPLICATION_JSON)
                .content(transactionJson))
        .andExpect(MockMvcResultMatchers.status().isOk());
  }

  @Test
  void testWithdraw() throws Exception {
    TransactionDto transactionDto = new TransactionDto();
    String transactionJson = convertToJson(transactionDto);

    mockMvc
        .perform(
            MockMvcRequestBuilders.post("/accounts/{accountId}/withdraw", UUID.randomUUID())
                .contentType(MediaType.APPLICATION_JSON)
                .content(transactionJson))
        .andExpect(MockMvcResultMatchers.status().isOk());
  }

  @Test
  void testGetBalance() throws Exception {
    mockMvc
        .perform(MockMvcRequestBuilders.get("/accounts/{accountId}/balance", UUID.randomUUID()))
        .andExpect(MockMvcResultMatchers.status().isOk());
  }

  @Test
  void testGetTransactionHistory() throws Exception {
    mockMvc
        .perform(
            MockMvcRequestBuilders.get("/accounts/{accountId}/transactions", UUID.randomUUID()))
        .andExpect(MockMvcResultMatchers.status().isOk());
  }

  private String convertToJson(Object obj) throws Exception {
    return objectMapper.writeValueAsString(obj);
  }
}
