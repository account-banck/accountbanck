package com.yassine.bankAccount.domain.services;

import com.yassine.bankAccount.application.incoming.AccountService;
import com.yassine.bankAccount.domain.model.AccountData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.UUID;
import java.util.stream.Stream;


@SpringBootTest
@ActiveProfiles("test")
@Sql(scripts = "/cleanup.sql")
@SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "/cleanup.sql"),
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"/data.sql"}),
})
class AccountServiceTest {

    @Autowired
    private AccountService accountService;

    private static Stream<UUID> provideAccountIds() {
        return Stream.of(
                UUID.fromString("123e4567-e89b-12d3-a456-556642440000"),
                UUID.fromString("223e4567-e89b-12d3-a456-556642440001"),
                UUID.randomUUID()
        );
    }

    @Test
    public void testCreateAccount() {
        var newAccountData = new AccountData();

        newAccountData.setAccountId(UUID.randomUUID());
        newAccountData.setBalance(new BigDecimal("1000.00"));
        newAccountData.setTransactions(new ArrayList<>());
        newAccountData.setOwnerName("John Doe");
        newAccountData.setAccountNumber("123456789");
        newAccountData.setAccountType("Checking");
        AccountData savedAccount = accountService.createAccount(newAccountData);
        Assertions.assertNotNull(savedAccount);
    }

    @Test
    public void testDeposit() {
        var accountId = UUID.fromString("123e4567-e89b-12d3-a456-556642440000");
        var initialBalance = accountService.getBalance(accountId);
        var depositAmount = new BigDecimal("100.00");
        accountService.deposit(accountId, depositAmount);
        var newBalance = accountService.getBalance(accountId);
        Assertions.assertEquals(initialBalance.add(depositAmount), newBalance);
        var transactionHistory=accountService.getTransactionHistory(accountId);
        Assertions.assertEquals(transactionHistory.size(),2);
    }

    @ParameterizedTest
    @ValueSource(doubles = {100.0, 200.0, -50.0, 0.0})
    public void testDepositWithVariousAmounts(double amountValue) {
        var accountId = UUID.fromString("123e4567-e89b-12d3-a456-556642440000");
        var initialBalance = accountService.getBalance(accountId);
        var depositAmount = new BigDecimal(amountValue);
        try {
            accountService.deposit(accountId, depositAmount);
            var newBalance = accountService.getBalance(accountId);
            if (amountValue > 0) {
                Assertions.assertEquals(initialBalance.add(depositAmount), newBalance);
            }
        } catch (RuntimeException e) {
            Assertions.assertTrue(amountValue <= 0);
        }
    }

    @Test
    public void testWithdraw() {
        var accountId = UUID.fromString("223e4567-e89b-12d3-a456-556642440001");
        BigDecimal initialBalance = accountService.getBalance(accountId);
        var withdrawalAmount = new BigDecimal("50.00");
        accountService.withdraw(accountId, withdrawalAmount);
        var newBalance = accountService.getBalance(accountId);
        Assertions.assertEquals(initialBalance.subtract(withdrawalAmount), newBalance);
        var transactionHistory=accountService.getTransactionHistory(accountId);
        Assertions.assertEquals(transactionHistory.size(),2);
    }

    @ParameterizedTest
    @ValueSource(doubles = {50.0, 150.0, -20.0, 5000.0})
    public void testWithdrawWithVariousAmounts(double amountValue) {
        var accountId = UUID.fromString("223e4567-e89b-12d3-a456-556642440001");
        var initialBalance = accountService.getBalance(accountId);
        var withdrawalAmount = new BigDecimal(amountValue);
        try {
            accountService.withdraw(accountId, withdrawalAmount);
            var newBalance = accountService.getBalance(accountId);
            if (amountValue > 0 && amountValue <= initialBalance.doubleValue()) {
                Assertions.assertEquals(initialBalance.subtract(withdrawalAmount), newBalance);
            }
        } catch (RuntimeException e) {
            Assertions.assertTrue(amountValue <= 0 || amountValue > initialBalance.doubleValue());
        }
    }

    @ParameterizedTest
    @MethodSource("provideAccountIds")
    public void testGetBalanceWithDifferentAccounts(UUID accountId) {
        try {
            var balance = accountService.getBalance(accountId);
            Assertions.assertNotNull(balance);
        } catch (RuntimeException e) {
            Assertions.assertEquals(e.getMessage(),"Compte non trouvé pour l'ID: "+accountId);
        }
    }

    @Test
    public void testGetBalance() {
        var accountId =   UUID.fromString("123e4567-e89b-12d3-a456-556642440000");
        var balance = accountService.getBalance(accountId);
        Assertions.assertNotNull(balance);
        BigDecimal expected = new BigDecimal("1000.00");
        BigDecimal actual =balance.setScale(2, RoundingMode.HALF_UP);
        Assertions.assertEquals(expected, actual);

    }

    @Test
    public void testGetTransactionHistory() {
        var accountId =  UUID.fromString("123e4567-e89b-12d3-a456-556642440000");
        var transactions = accountService.getTransactionHistory(accountId);
        Assertions.assertNotNull(transactions);

    }

}
