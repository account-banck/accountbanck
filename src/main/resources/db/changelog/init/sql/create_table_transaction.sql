--liquibase formatted sql
--changeset author:unique_id_001
create table transaction
(
    transaction_id varchar(36)                         not null
        primary key,
    account_id     varchar(36)                         null,
    date           date                                null,
    amount         decimal(38, 2)                      null,
    description    varchar(255)                        null,
    type           varchar(255)                        null,
    created        timestamp default CURRENT_TIMESTAMP not null,
    updated        timestamp default CURRENT_TIMESTAMP not null,
    constraint transaction_ibfk_1
        foreign key (account_id) references account (account_id),
    constraint type_enum_check check (type in ('DEPOT', 'RETRAIT'))
);
