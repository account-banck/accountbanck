--liquibase formatted sql
--changeset author:unique_id_002
create table account
(
    account_id     varchar(36) not null primary key,
    balance        decimal(38, 2) not null,
    owner_name     varchar(255) null,
    account_number varchar(255) null,
    account_type   varchar(255) null,
    created        timestamp default CURRENT_TIMESTAMP not null,
    updated        timestamp default CURRENT_TIMESTAMP not null
);

