package com.yassine.bankAccount.infrastructure.persistence.repository;

import com.yassine.bankAccount.infrastructure.persistence.entity.AccountJpa;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountDataAccess extends JpaRepository<AccountJpa, String> {

  AccountJpa save(AccountJpa accountJpa);

  Optional<AccountJpa> findAccountJpaByAccountId(String accountId);
}
