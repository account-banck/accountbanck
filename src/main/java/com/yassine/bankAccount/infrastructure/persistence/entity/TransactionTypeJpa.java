package com.yassine.bankAccount.infrastructure.persistence.entity;

public enum TransactionTypeJpa {
  DEPOT,
  RETRAIT
}
