package com.yassine.bankAccount.infrastructure.persistence.repository;

import com.yassine.bankAccount.infrastructure.persistence.entity.TransactionJpa;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionDataAccess extends JpaRepository<TransactionJpa, String> {

  TransactionJpa save(TransactionJpa transactionJpa);

  Optional<TransactionJpa> findTransactionByTransactionId(String transactionId);
}
