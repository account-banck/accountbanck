package com.yassine.bankAccount.infrastructure.persistence.entity;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.util.List;

import lombok.*;
import org.hibernate.annotations.UuidGenerator;

@Getter
@Setter
@Table(name = "account")
@Entity
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
public class AccountJpa extends BaseJpa {

  @Id
  @UuidGenerator
  @Column(unique = true, length = 36)
  private String accountId;

  @Column(nullable = false)
  private BigDecimal balance;

  @OneToMany(mappedBy = "account", cascade = CascadeType.ALL)
  private List<TransactionJpa> transactions;

  @Column private String ownerName;
  @Column private String accountNumber;
  @Column private String accountType;
}
