package com.yassine.bankAccount.infrastructure.persistence.entity;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

import lombok.*;
import org.hibernate.annotations.UuidGenerator;



@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "transaction")
@Entity
public class TransactionJpa extends BaseJpa {
  @Id
  @UuidGenerator
  @Column(unique = true, length = 36)
  private String transactionId;

  @ManyToOne
  @JoinColumn(name = "account_id")
  private AccountJpa account;

  @Column private LocalDate date;
  @Column private BigDecimal amount;
  @Column private String description;

  @Column
  @Enumerated(EnumType.STRING)
  private TransactionTypeJpa type;

}
