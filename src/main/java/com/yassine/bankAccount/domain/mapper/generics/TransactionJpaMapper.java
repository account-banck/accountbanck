package com.yassine.bankAccount.domain.mapper.generics;

import com.yassine.bankAccount.infrastructure.persistence.entity.TransactionJpa;
import com.yassine.bankAccount.domain.mapper.GenericJpaMapper;
import com.yassine.bankAccount.domain.model.TransactionData;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TransactionJpaMapper extends GenericJpaMapper<TransactionData, TransactionJpa> {}
