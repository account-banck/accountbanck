package com.yassine.bankAccount.domain.mapper.generics;

import com.yassine.bankAccount.infrastructure.persistence.entity.AccountJpa;
import com.yassine.bankAccount.domain.mapper.GenericJpaMapper;
import com.yassine.bankAccount.domain.model.AccountData;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AccountJpaMapper extends GenericJpaMapper<AccountData, AccountJpa> {}
