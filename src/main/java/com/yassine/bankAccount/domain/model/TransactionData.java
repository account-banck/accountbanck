package com.yassine.bankAccount.domain.model;

import java.math.BigDecimal;
import java.util.UUID;
import lombok.*;

@Getter
@Setter
@RequiredArgsConstructor
public class TransactionData {
  private UUID transactionId;
  private UUID accountId;
  private BigDecimal amount;
  private String description;
  private TransactionTypeData type;
}
