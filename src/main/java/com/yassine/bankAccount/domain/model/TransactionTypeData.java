package com.yassine.bankAccount.domain.model;

public enum TransactionTypeData {
  DEPOT,
  RETRAIT
}
