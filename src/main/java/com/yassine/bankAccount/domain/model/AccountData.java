package com.yassine.bankAccount.domain.model;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;
import lombok.*;

@Getter
@Setter
@RequiredArgsConstructor
public class AccountData {
  private UUID accountId;
  private BigDecimal balance;
  private List<TransactionData> transactions;
  private String ownerName;
  private String accountNumber;
  private String accountType;
}
