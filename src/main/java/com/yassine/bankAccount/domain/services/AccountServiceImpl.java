package com.yassine.bankAccount.domain.services;

import com.yassine.bankAccount.common.MapperService;
import com.yassine.bankAccount.infrastructure.persistence.entity.AccountJpa;
import com.yassine.bankAccount.infrastructure.persistence.entity.TransactionJpa;
import com.yassine.bankAccount.infrastructure.persistence.entity.TransactionTypeJpa;
import com.yassine.bankAccount.domain.mapper.generics.AccountJpaMapper;
import com.yassine.bankAccount.domain.mapper.generics.TransactionJpaMapper;
import com.yassine.bankAccount.domain.model.AccountData;
import com.yassine.bankAccount.domain.model.TransactionData;
import com.yassine.bankAccount.application.incoming.AccountService;
import com.yassine.bankAccount.infrastructure.persistence.repository.AccountDataAccess;
import com.yassine.bankAccount.infrastructure.persistence.repository.TransactionDataAccess;
import jakarta.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Transactional
@Slf4j
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

  private final AccountDataAccess accountDataAccess;
  private final TransactionDataAccess transactionDataAccess;
  private final MapperService mapperService;

  @Override
  public List<AccountData> getAllAccounts() {
    var mapper = mapperService.getByType(AccountJpaMapper.class);
    var account = accountDataAccess.findAll();
    return mapper.toDomain(account);
  }
  @Override
  public AccountData createAccount(AccountData data) {
    data.setTransactions(List.of());
    var mapper = mapperService.getByType(AccountJpaMapper.class);
    var account = accountDataAccess.save(mapper.toJpa(data));
    return mapper.toDomain(account);
  }

  @Override
  public void deposit(UUID accountId, BigDecimal amount) {
    var optionalAccount = accountDataAccess.findById(accountId.toString());
    if (optionalAccount.isPresent() && amount.compareTo(BigDecimal.ZERO) > 0) {
      var account = optionalAccount.get();
      account.setBalance(account.getBalance().add(amount));
      var transationCreated = saveTransaction(amount, account);
      account.getTransactions().add(transationCreated);
      accountDataAccess.save(account);
    } else if (amount.compareTo(BigDecimal.ZERO) <= 0) {
      throw new RuntimeException("Le montant saisi n'est pas valide pour une opération bancaire.");
    } else {
      throw new RuntimeException("L'identifiant du compte ne correspond à aucun compte.");
    }
  }

  @Override
  public void withdraw(UUID accountId, BigDecimal amount) {
    var optionalAccount = accountDataAccess.findById(accountId.toString());
    if (optionalAccount.isPresent() && amount.compareTo(BigDecimal.ZERO) > 0) {
      var account = optionalAccount.get();
      if (account.getBalance().compareTo(amount) >= 0) {
        account.setBalance(account.getBalance().subtract(amount));
        var transationCreated = saveTransaction(amount.negate(), account);
        account.getTransactions().add(transationCreated);
        accountDataAccess.save(account);
      } else {
        throw new RuntimeException("Le solde du compte est insuffisant pour ce retrait.");
      }
    } else if (amount.compareTo(BigDecimal.ZERO) <= 0) {
      throw new RuntimeException("Le montant saisi n'est pas valide pour une opération bancaire.");
    } else {
      throw new RuntimeException("L'identifiant du compte ne correspond à aucun compte.");
    }
  }

  @Override
  public BigDecimal getBalance(UUID accountId) {
    return accountDataAccess
        .findAccountJpaByAccountId(accountId.toString())
        .map(AccountJpa::getBalance)
        .orElseThrow(() -> new RuntimeException("Compte non trouvé pour l'ID: " + accountId));
  }

  @Override
  public List<TransactionData> getTransactionHistory(UUID accountId) {
    var optionalAccount = accountDataAccess.findAccountJpaByAccountId(accountId.toString());
    if (optionalAccount.isPresent()) {
      var mapper = mapperService.getByType(TransactionJpaMapper.class);
      var account = optionalAccount.get();
      return mapper.toDomain(account.getTransactions());
    } else {
      throw new RuntimeException("L'identifiant du compte ne correspond à aucun compte.");
    }
  }

  private TransactionJpa saveTransaction(BigDecimal amount, AccountJpa account) {
    var transaction =  TransactionJpa.builder()
            .account(account)
            .type(TransactionTypeJpa.DEPOT)
            .amount(amount)
            .description("Depot de l'argent")
            .build();
    return transactionDataAccess.save(transaction);
  }
}
