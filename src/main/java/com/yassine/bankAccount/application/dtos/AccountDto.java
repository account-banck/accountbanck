package com.yassine.bankAccount.application.dtos;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;
import lombok.*;

@Getter
@Setter
@RequiredArgsConstructor
public class AccountDto {
  private UUID accountId;
  private BigDecimal balance;
  private List<TransactionDto> transactions;
  private String ownerName;
  private String accountNumber;
  private String accountType;
}
