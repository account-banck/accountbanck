package com.yassine.bankAccount.application.dtos;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;
import lombok.*;

@Getter
@Setter
@RequiredArgsConstructor
public class TransactionDto {
  private UUID transactionId;
  private LocalDate date;
  private BigDecimal amount;
  private String description;
  private TransactionTypeDto type;
}
