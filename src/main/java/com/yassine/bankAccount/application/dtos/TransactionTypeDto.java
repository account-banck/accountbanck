package com.yassine.bankAccount.application.dtos;

public enum TransactionTypeDto {
  DEPOT,
  RETRAIT
}
