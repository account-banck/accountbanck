package com.yassine.bankAccount.application.incoming;

import com.yassine.bankAccount.domain.model.AccountData;
import com.yassine.bankAccount.domain.model.TransactionData;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public interface AccountService {

  List<AccountData> getAllAccounts();
  AccountData createAccount(AccountData data);

  void deposit(UUID accountId, BigDecimal amount);

  void withdraw(UUID accountId, BigDecimal amount);

  BigDecimal getBalance(UUID accountId);

  List<TransactionData> getTransactionHistory(UUID accountId);
}
