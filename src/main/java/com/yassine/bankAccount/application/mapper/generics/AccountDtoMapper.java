package com.yassine.bankAccount.application.mapper.generics;

import com.yassine.bankAccount.application.dtos.AccountDto;
import com.yassine.bankAccount.application.mapper.GenericDtoMapper;
import com.yassine.bankAccount.domain.model.AccountData;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AccountDtoMapper extends GenericDtoMapper<AccountData, AccountDto> {}
