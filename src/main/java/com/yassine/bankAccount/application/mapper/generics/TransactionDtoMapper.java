package com.yassine.bankAccount.application.mapper.generics;

import com.yassine.bankAccount.application.dtos.TransactionDto;
import com.yassine.bankAccount.application.mapper.GenericDtoMapper;
import com.yassine.bankAccount.domain.model.TransactionData;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TransactionDtoMapper extends GenericDtoMapper<TransactionData, TransactionDto> {}
