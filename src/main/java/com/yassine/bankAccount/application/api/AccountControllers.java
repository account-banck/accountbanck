package com.yassine.bankAccount.application.api;

import com.yassine.bankAccount.application.dtos.AccountDto;
import com.yassine.bankAccount.application.dtos.TransactionDto;
import com.yassine.bankAccount.application.mapper.generics.AccountDtoMapper;
import com.yassine.bankAccount.application.mapper.generics.TransactionDtoMapper;
import com.yassine.bankAccount.common.MapperService;
import com.yassine.bankAccount.application.incoming.AccountService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/accounts", produces = MediaType.APPLICATION_JSON_VALUE)
public class AccountControllers {

  private final AccountService accountService;
  private final MapperService mapperService;

  @PostMapping
  @Operation(summary = "Créer un compte", description = "Crée un nouveau compte bancaire")
  @ApiResponse(
      responseCode = "201",
      description = "Compte créé avec succès",
      content =
          @Content(
              mediaType = "application/json",
              schema = @Schema(implementation = AccountDto.class)))
  public ResponseEntity<AccountDto> createAccount(@RequestBody AccountDto accountDto) {
    var mapper = mapperService.getByType(AccountDtoMapper.class);
    var account = accountService.createAccount(mapper.toDomain(accountDto));
    return new ResponseEntity<>(mapper.toDto(account), HttpStatus.CREATED);
  }
    @GetMapping
    @Operation(summary = "Liste de tous les comptes", description = "Récupère une liste de tous les comptes bancaires")
    public ResponseEntity<List<AccountDto>> getAllAccounts() {
        var mapper = mapperService.getByType(AccountDtoMapper.class);
        var accounts = accountService.getAllAccounts();
        return ResponseEntity.ok(mapper.toDto(accounts));
    }

  @PostMapping("/{accountId}/deposit")
  @Operation(summary = "Dépôt", description = "Dépose de l'argent sur un compte spécifique")
  public ResponseEntity<Void> deposit(
      @Parameter(description = "ID du compte") @PathVariable UUID accountId,
      @RequestBody TransactionDto transactionDto) {
    accountService.deposit(accountId, transactionDto.getAmount());
    return ResponseEntity.ok().build();
  }

  @PostMapping("/{accountId}/withdraw")
  @Operation(summary = "Retrait", description = "Retire de l'argent d'un compte spécifique")
  public ResponseEntity<Void> withdraw(
      @Parameter(description = "ID du compte") @PathVariable UUID accountId,
      @RequestBody TransactionDto transactionDto) {
    accountService.withdraw(accountId, transactionDto.getAmount());
    return ResponseEntity.ok().build();
  }

  @GetMapping("/{accountId}/balance")
  @Operation(summary = "Solde du compte", description = "Obtient le solde d'un compte spécifique")
  public ResponseEntity<BigDecimal> getBalance(
      @Parameter(description = "ID du compte") @PathVariable UUID accountId) {
    BigDecimal balance = accountService.getBalance(accountId);
    return ResponseEntity.ok(balance);
  }

  @GetMapping("/{accountId}/transactions")
  @Operation(
      summary = "Historique des transactions",
      description = "Obtient l'historique des transactions d'un compte")
  public ResponseEntity<List<TransactionDto>> getTransactionHistory(
      @Parameter(description = "ID du compte") @PathVariable UUID accountId) {
    var mapper = mapperService.getByType(TransactionDtoMapper.class);
    var transactions = mapper.toDto(accountService.getTransactionHistory(accountId));
    return ResponseEntity.ok(transactions);
  }
}
